const path = require("path");

var config = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "main.js"
    },
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.join(__dirname, "src"),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-proposal-object-rest-spread']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        },
                    },
                    {
                        loader: 'postcss-loader', options: {
                            plugins: function () {
                                return [require('autoprefixer')];
                            }
                        }
                    },
                    'sass-loader',
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {},
                    },
                ],
            },
            {
                test: /\.txt$/i,
                use: 'raw-loader',
            },
        ]
    },
    optimization: {
        minimize: true,
    },
    plugins: [
    ],
    devServer: {
        contentBase: path.resolve(__dirname, "./dist"),
        port: 9000,
        open: true,
        hot: true,
        progress: true
    },
    //devtool: "eval-source-map"
};

module.exports = config;