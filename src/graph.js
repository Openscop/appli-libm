class Graph {

    set maxValue(value) {
        this._maxValue = value;
    }

    constructor(canvas, side = "left") {

        this._canvas = canvas
        this._maxValue = 100;

        this.expandSize();

        this.colorGrey = "#606060";
        this.colorWhiteTransparent = "rgba(255, 255, 255, 0.8)";

        if (side == "left") {
            this.color = "#23d1e2";
            this.colorLight = "#2191A2";
            this.textColor = "#0d222d";
        } else {
            this.color = "#33ffe8";
            this.colorLight = "#32dac3";
            this.textColor = "#175649";
        }

        if (canvas.getContext) {
            this.ctx = canvas.getContext('2d');
            this.clear();
        } else {
            alert("Votre navigateur est trop ancien. Veuillez le mettre à jour pour utiliser ce simulateur.");
            return none;
        }
    };

    _convertValueToPixel(value) {
        return value * this.height / this._maxValue
    }

    expandSize() {
        let size = this._canvas.parentElement.getBoundingClientRect();
        this.height = size.height - 20 ;
        this.width = size.width / 2 - 10;
        this._canvas.height = this.height;
        this._canvas.width = this.width;
        return size;
    }

    clear() {
        this.ctx.clearRect(0, 0, this.width, this.height);
        this.ctx.strokeStyle = this.colorLight;
        this.ctx.lineWidth = 2;
        this.ctx.setLineDash([0, 0]);
        this.ctx.fillStyle = "#ffffff";
        this.ctx.strokeRect(0, 0, this.width, this.height);
    }

    addBar(value) {
        let pxValue = this._convertValueToPixel(value);
        this.ctx.lineWidth = 2;
        this.ctx.fillStyle = this.colorLight;
        this.ctx.fillRect(1, this.height - pxValue, this.width - 2, pxValue - 1);

    }

    addValueText(value) {
        let pxValue = this._convertValueToPixel(value);

            let text = String(Number.parseFloat(value).toFixed(1));
            this.ctx.font = '20px arial';
            // white background
            this.ctx.fillStyle = this.colorWhiteTransparent;
            //let y = (textPosition=="top") ? this.height - pxValue - 5 : this.height - pxValue + 16;
            let y = (pxValue < this.height * 0.91) ? this.height - pxValue - 3 : this.height - pxValue + 18;
            let x = this.width - this.ctx.measureText(text).width - 5;
            this.ctx.fillRect(x - 1, y-18, this.ctx.measureText(text).width + 2, 19);
            // text
            this.ctx.fillStyle = this.textColor;
            this.ctx.fillText(text, x, y);

    }

    addLine(value, text = null, textPosition = "top") {
        let pxValue = this._convertValueToPixel(value);
        this.ctx.lineWidth = 2;
        this.ctx.setLineDash([2, 2]);
        this.ctx.strokeStyle = this.colorGrey;
        this.ctx.beginPath();
        this.ctx.moveTo(0, this.height - pxValue);
        this.ctx.lineTo(this.width, this.height - pxValue);
        this.ctx.stroke();

        if (text) {
            this.ctx.font = '14px arial';
            // white background
            this.ctx.fillStyle = this.colorWhiteTransparent;
            let y = (textPosition=="top") ? this.height - pxValue - 3 : this.height - pxValue + 15;
            this.ctx.fillRect(5 - 1, y - 12, this.ctx.measureText(text).width + 2, 13);
            // text
            this.ctx.fillStyle = this.colorGrey;
            this.ctx.fillText(text, 5, y);
        }
    }

};


export default Graph;