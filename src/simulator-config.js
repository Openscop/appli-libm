let simulatorConfig = {
    profil: {
        homme: {
            VO2max: [70, 80],
            seuil4mM: [0.8, 0.9],
            coutNetO2: [0.21, 0.19],
            wingate: [10, 11],
        },
        femme: {
            VO2max: [60, 70],
            seuil4mM: [0.8, 0.9],
            coutNetO2: [0.19, 0.21],
            wingate: [9, 10],
        }
    },
    csvColumn: {
        code: ["distance", "VO2max", "seuil4mM", "coutNetO2", "wingate", "VMA", "vitesseSeuil4mM", "tempsCourseSec", "tempsCourseMin", "vitesseMoyenneMsec", "vitesseMoyenneKMh", "vitesse100pVMA", "VO2moyenMlMinKg", "VO2moyen100pVO2max", "VO2finCourseMlMinKg", "VO2finCourse100pVO2max", "lactateFinCourse", "aerobie", "anaerobie"],
        label: ["distance", "VO2max (ml/min/kg)", "seuil 4mM (% VO2max)", "Cout net O2 à 12km/h", "Puissance moyenne Wingate (W/kg)", "VMA (km/h)", "Vitesse au seuil 4mM (km/h)", "Temps de course (sec)", "Temps de course (min)", "Vitesse moyenne (m/sec)", "Vitesse moyenne (km/h)", "Vitesse (% vma)", "VO2 moyen (ml/min/kg)", "VO2 moyen (%VO2max)", "VO2 fin de course (ml/min/kg)", "VO2 fin de course (%VO2max)", "Lactate fin de course (mM)", "Energie aérobie (% total)", "Energie anaérobie (% total)"],
        labelCourt: ["distance", "VO2max", "seuil 4mM", "Cout net O2", "Wingate", "VMA", "seuil 4mM", "Tps course", "Tps course", "Vit. moy. (m/sec)", "Vit. moy. (km/h)", "Vit. (% vma)", "VO2 moyen (ml/min/kg)", "VO2 moyen (%VO2max)", "VO2 fin de course (ml/min/kg)", "VO2 fin de course (%VO2max)", "Lactate fin de course (mM)", "Energie aérobie (% total)", "Energie anaérobie (% total)"],
    },
    performance: {
        homme: null,
        femme: null,
    },
    graphMaxValue: {
        vitesseMoyenneKMh: 35,
        vitesse100pVMA: 175,
        VO2moyenMlMinKg: 85,
        VO2moyen100pVO2max: 100,
        VO2finCourseMlMinKg: 85,
        VO2finCourse100pVO2max: 100,
        lactateFinCourse: 20,
        aerobie : 100,
        anaerobie: 100
    }
}

export default simulatorConfig;